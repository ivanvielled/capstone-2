console.log('"Say hello to my lil friend" -Tony Montana')

let loginForm = document.querySelector('#logInUser')
let token = localStorage.getItem('token')

navItems.innerHTML = 
`
	<ul class="navbar-nav mx-auto">
		<li class="nav-item">
			<a href="./register.html" class="nav-link">Register</a>
		</li>

		<li class="nav-item active">
			<a href="./login.html" class="nav-link">Login</a>
		</li>
	</ul>
`

if (token) {
	window.location.replace('./courses.html')
}

loginForm.addEventListener('submit', (e) => {
	e.preventDefault()

	let username = document.querySelector('#username').value
	let password = document.querySelector('#password').value

	if (username === "" || password === "") {
		alert("Please fill all the required fields.")
	} else {
		fetch('https://csp2-dupaya.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				username: username,
				password: password
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if (data.access) {
				localStorage.setItem('token', data.access)
				fetch('https://csp2-dupaya.herokuapp.com/users/user-details', {
					headers: {
						Authorization: `Bearer ${data.access}`
					}
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					localStorage.setItem('id', data.resultFromFindById._id)
					localStorage.setItem('isAdmin', data.resultFromFindById.isAdmin)
					window.location.replace('./courses.html')
				})
			} else {
				alert("Incorrect login credentials.")
			}
		})
	}
})