let navItems = document.querySelector('#navSession')
let navIndex = document.querySelector('#indexSession')
let footer = document.querySelector('#footerContainer')
let navbar = document.querySelector('#navbar-main')
let navLink = document.querySelectorAll('.nav-link')
let userToken = localStorage.getItem('token')
console.log(userToken)

if (!userToken) {
	navItems.innerHTML = 
	`
		<ul class="navbar-nav mx-auto">
			<li class="nav-item">
				<a href="./register.html" class="nav-link">Register</a>
			</li>

			<li class="nav-item">
				<a href="./login.html" class="nav-link">Login</a>
			</li>
		</ul>
	`
} else {
	navItems.innerHTML = 
	`
		<ul class="navbar-nav mx-auto">
			<li class="nav-item">
				<a href="./profile.html" class="nav-link")">Profile</a>
			</li>

			<li class="nav-item">
				<a href="./logout.html" class="nav-link")">Logout</a>
			</li>
		</ul>
	`
}