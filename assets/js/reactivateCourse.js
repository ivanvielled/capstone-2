console.log('"Say hello to my lil friend" -Tony Montana')

let params = new URLSearchParams(window.location.search)

let courseId = params.get('courseId')
let token = localStorage.getItem('token')

fetch(`https://csp2-dupaya.herokuapp.com/courses/reactivate/${courseId}`, {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json',
		Authorization: `Bearer ${token}`
	},
	body: JSON.stringify({
		isActive: true
	})
})
	.then(res => {
		return res.json()
	})
	.then(data => {
		if (data === true) {
			window.location.replace('./courses.html')
		} else {
			alert("Something went wrong :c")
	}
})
