console.log('"Say hello to my lil friend" -Tony Montana')

let registerForm = document.querySelector('#registerUser')
let token = localStorage.getItem('token')

navItems.innerHTML = 
`
	<ul class="navbar-nav mx-auto">
		<li class="nav-item active">
			<a href="./register.html" class="nav-link">Register</a>
		</li>

		<li class="nav-item">
			<a href="./login.html" class="nav-link">Login</a>
		</li>
	</ul>
`

registerForm.addEventListener("submit", (e) => {
	e.preventDefault()

	let firstName = document.querySelector('#firstName').value
	let lastName = document.querySelector('#lastName').value
	let mobileNo = document.querySelector('#mobileNumber').value
	let email = document.querySelector('#userEmail').value
	let username = document.querySelector('#username').value
	let password1 = document.querySelector('#password1').value
	let password2 = document.querySelector('#password2').value

	//validation
	if ((password1 !== "" && password2 !== "") && (password1 === password2) && (mobileNo.length === 11)) {

		//check for duplicate email/username first
		fetch('https://csp2-dupaya.herokuapp.com/users/user-exists', {
			method: 'POST',
			headers: {
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				username: username
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data === false) {
				fetch('https://csp2-dupaya.herokuapp.com/users', {
					method: 'POST',
					headers: {
						'Content-type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo,
						email: email,
						username: username,
						password: password1
					})
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					console.log(data)
					if (data === true) {
						alert("Registration successful!")
						window.location.replace('./login.html')
					} else {
						alert("Registration failed :c")
					}
				})
			} else {
				alert("Email/Username already exists!")
			}
		})
	} else {
		alert("Something went wrong :c")
	}
})