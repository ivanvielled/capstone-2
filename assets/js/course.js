console.log('"Say hello to my lil friend" -Tony Montana')

let params = new URLSearchParams(window.location.search)

let courseId = params.get('courseId')
let token = localStorage.getItem('token')
let userId = localStorage.getItem('id')
let adminUser = localStorage.getItem("isAdmin")

let courseName = document.querySelector('#courseName')
let courseDesc = document.querySelector('#courseDesc')
let coursePrice = document.querySelector('#coursePrice')
let enrollContainer = document.querySelector('#enrollContainer')
let enrolleesList = document.querySelector('#enrolleesList')
let enrolleesBody = document.querySelector('#enrolleesBody')
// let enrolleeName = document.querySelector('#enrolleeName')
// let enrolleeEmail = document.querySelector('#enrolleeEmail')
// let enrolleeMobile = document.querySelector('#enrolleeMobile')

console.log(adminUser)

fetch(`https://csp2-dupaya.herokuapp.com/courses/${courseId}`)
.then(res => {
	return res.json()
})
.then(data => {
	// console.log(courseId)
	console.log(data.resultFromEnrollees)
	// console.log(data.resultFromEnrollees.courseId)
	// console.log(data)
	courseName.innerHTML = data.resultFromFindById.name,
	courseDesc.innerHTML = data.resultFromFindById.description,
	coursePrice.innerHTML = data.resultFromFindById.price

	if (!token) {
		enrollContainer.innerHTML = 
		`
			<a href="./login.html">Login to enroll</a>
		`
	} else {
		if (adminUser == 'false' || !adminUser) {
			enrollContainer.innerHTML = 
				`
					<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>
				`

			enrolleesList.innerHTML = null

			let enrollButton = document.querySelector('#enrollButton')
				enrollButton.addEventListener('click', (e) => {
					e.preventDefault()
					fetch('https://csp2-dupaya.herokuapp.com/users/enroll', {
						method: 'PUT',
						headers: {
							'Content-type': 'application/json',
							Authorization: `Bearer ${token}`
						},
						body: JSON.stringify({
							userId: userId,
							courseId: courseId
						})
					})
					.then(res => {
						return res.json()
					})
					.then(data => {
						if (data === true) {
							alert("Thank you for enrolling!")
							window.location.replace('./courses.html')
						} else {
							alert("Something went wrong :c")
						}
					})
				})
			} else {
				let enrollees = data.resultFromEnrollees.map(enrolleesResult => {
					return (
							`
								<tr>
									<td>${enrolleesResult.firstName} ${enrolleesResult.lastName}</td>
									<td>${enrolleesResult.email}</td>
									<td>${enrolleesResult.mobileNo}</td>				
								</tr>				
							`
						)
					
						// enrolleeName.innerHTML = 
						// `
						// 	${enrolleesResult.firstName} ${enrolleesResult.lastName}
						// `

						// enrolleeEmail.innerHTML = 
						// `
						// 	${enrolleesResult.email}
						// `

						// enrolleeMobile.innerHTML = 
						// `
						// 	${enrolleesResult.mobileNo}
						// `
				}).join("")
				enrolleesBody.innerHTML = 
				`
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead class="thead-dark">
							<tr>
								<th scope="col">Name</th>
								<th scope="col">Email</th>
								<th scope="col">Mobile number</th>
							</tr>
						</thead>

						<tbody>
							${enrollees}
						</tbody>
					</table>
				</div>
				`
			}

			let enrollmentStatus = data.resultFromEnrollees.map(status => {
				// console.log(status)
				if (userId == status._id) {
					enrollContainer.innerHTML =
					`
						<h5>Enrolled</h5>
					`
				}
			})
	}
})
