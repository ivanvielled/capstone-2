console.log('"Say hello to my lil friend" -Tony Montana')

let adminUser = localStorage.getItem("isAdmin")
let modalButton = document.querySelector('#adminButton')
let token = localStorage.getItem("token")
let cardFooter;

console.log(adminUser)

if (adminUser == 'false' || !adminUser) {
	modalButton.innerHTML = null
} else {
	modalButton.innerHTML =
	`
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a>
		</div>
	`
}

if (adminUser == 'false' || !adminUser) {
	fetch('https://csp2-dupaya.herokuapp.com/courses/user')
	.then(res => res.json())
	.then(data => {
		console.log(data)
		let courseData;

		if (data.length < 1) {
			courseData = "No course available"
		} else {
			courseData = data.map(course => {

				cardFooter =
				`
					<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block editButton">Select Course</a>
				`

				return (
						`
							<div class="col-md-6 my-3">
								<div class="card bg-transparent">
									<div class="card-body">
										<h5 class="card-title">${course.name}</h5>
										<p class="card-text text-left">${course.description}</p>
										<p class="card-text text-right">₱ ${course.price}</p>
									</div>

									<div class="card-footer">
										${cardFooter}
									</div>
								</div>
							</div>
						`
					)
			}).join("")
		}

		let container = document.querySelector('#coursesContainer')

		container.innerHTML = courseData
	})
} else {
	fetch('https://csp2-dupaya.herokuapp.com/courses/admin')
	.then(res => res.json())
	.then(data => {
		let courseData;

		if (data.length < 1) {
			courseData = "No course available"
		} else {
			courseData = data.map(course => {
				if (!course.isActive) {
					cardFooter = 
					`
						<div class="anchor-block d-flex">
							<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-default text-primary btn-block viewButton"> View </a>

							<a href="./editCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-default text-success btn-block editButton"> Edit </a>

							<a href="./reactivateCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-default text-success btn-block reactivateButton"> Reactivate </a>
						</div>

					`
				} else {
					cardFooter = 
					`
						<div class="anchor-block d-flex">
							<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-default text-primary btn-block viewButton"> View </a>

							<a href="./editCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-default text-success btn-block editButton">Edit</a>

							<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-default text-danger btn-block deleteButton">Delete</a>
						</div>
					`
				}
				
				if (!course.isActive) {
					return (
						
							`
								<div class="col-md-6 my-3">
									<div class="card">
										<div class="card-body">
											<h5 class="card-title">${course.name}</h5>
											<p class="card-text text-left">${course.description}</p>
											<p class="card-text text-right">₱ ${course.price}</p>
											<p class="card-text text-left">Status: Inactive
										</div>

										<div class="card-footer">
											${cardFooter}
										</div>
									</div>
								</div>
							`
						)
				} else {
					return (
						
							`
								<div class="col-md-6 my-3">
									<div class="card">
										<div class="card-body">
											<h5 class="card-title">${course.name}</h5>
											<p class="card-text text-left">${course.description}</p>
											<p class="card-text text-right">₱ ${course.price}</p>
											<p class="card-text text-left">Status: Active
										</div>

										<div class="card-footer">
											${cardFooter}
										</div>
									</div>
								</div>
							`
					)
				}
				
			}).join("")
		}

		let container = document.querySelector('#coursesContainer')

		if (container == null) {
			return (
				container.innerHTML =
					`
						<h5>No courses available</h5>
					`
				)
		} else {
			container.innerHTML = courseData
		}
	})
}