console.log('"Say hello to my lil friend" -Tony Montana')

let params = new URLSearchParams(window.location.search)
// console.log(params)

let courseId = params.get('courseId')
let name = document.querySelector('#courseName')
let description = document.querySelector('#courseDescription')
let price = document.querySelector('#coursePrice')

fetch(`https://csp2-dupaya.herokuapp.com/courses/${courseId}`)
.then(res => {
	return res.json()
})
.then(data => {
	console.log(data)
	let editCourse = document.querySelector('#editCourse')

	name.placeholder = data.resultFromFindById.name
	description.placeholder = data.resultFromFindById.description
	price.placeholder = data.resultFromFindById.price

	editCourse.addEventListener('submit', (e) => {
		e.preventDefault()

		let courseName = name.value
		let courseDescription = description.value
		let coursePrice = price.value

		let token = localStorage.getItem('token')

		fetch('https://csp2-dupaya.herokuapp.com/courses', {
			method: 'PUT',
			headers: {
				'Content-type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: courseId,
				name: courseName,
				description: courseDescription,
				price: coursePrice
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if (data === true) {
				window.location.replace('./courses.html')
			} else {
				alert("Something went wrong :c")
			}
		})
	})
})