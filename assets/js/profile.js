console.log('"Say hello to my lil friend" -Tony Montana')

let token = localStorage.getItem('token')

if (!token) {
	window.location.replace('./login.html')
} else {
	navItems.innerHTML = 
	`
		<ul class="navbar-nav mx-auto">
			<li class="nav-item active">
				<a href="./profile.html" class="nav-link")">Profile</a>
			</li>

			<li class="nav-item">
				<a href="./logout.html" class="nav-link")">Logout</a>
			</li>
		</ul>
	`
}

fetch('https://csp2-dupaya.herokuapp.com/users/user-details', {
	headers: {
		Authorization: `Bearer ${token}`
	}
})
.then(res => {
	return res.json()
})
.then(data => {
	console.log(data)
	let container = document.querySelector('#profileContainer')
	let courses = document.querySelector('#coursesContainer')

	if (data.resultFromFindById.isActive === true) {
		if (data.resultFromFindById.isAdmin === true) {
			container.innerHTML =
			`
				<div class="col-md-12 my-3">
					<div class="card border">
						<div class="card-body">
							<h5 class="card-title"><b><u>User Profile</u></b></h5>
							<p class="card-text text-left">Username: <b>${data.resultFromFindById.username}</b></p>
							<p class="card-text text-left">Email: <b>${data.resultFromFindById.email}</b></p>
							<p class="card-text text-left">First name: <b>${data.resultFromFindById.firstName}</b></p>
							<p class="card-text text-left">Last name: <b>${data.resultFromFindById.lastName}</b></p>
							<p class="card-text text-left">Mobile number: <b>${data.resultFromFindById.mobileNo}</b></p>
							<p class="card-text text-right">Status: <b>Active</b></p>
						</div>
					</div>
				</div>
			`

			coursesContainer.innerHTML = null

		} else {
			container.innerHTML =
			`
				<div class="col-md-12 my-3">
					<div class="card border">
						<div class="card-body">
							<h5 class="card-title"><b><u>User Profile</u></b></h5>
							<p class="card-text text-left">Username: <b>${data.resultFromFindById.username}</b></p>
							<p class="card-text text-left">Email: <b>${data.resultFromFindById.email}</b></p>
							<p class="card-text text-left">First name: <b>${data.resultFromFindById.firstName}</b></p>
							<p class="card-text text-left">Last name: <b>${data.resultFromFindById.lastName}</b></p>
							<p class="card-text text-left">Mobile number: <b>${data.resultFromFindById.mobileNo}</b></p>
							<p class="card-text text-right">Status: <b>Active</b></p>
						</div>
					</div>
				</div>
			`

			if (data.resultFromFindCourses.length < 1) {
				enrollmentsData = "Not enrolled"
			} else {
				let enrollmentsData;

				enrollmentsData = data.resultFromFindCourses.map(enrolledCourses => {
					return (
						`
							<ul class="justify-content-center ml-3">
								<li class="text-left">${enrolledCourses.name}</li>
							</ul>
								
						`
					)
					// console.log(enrolledCourses)
				}).join("")
				// console.log(enrollmentsData)
				enrolledCourses.innerHTML = enrollmentsData
			}
		}	

	} else {
		container.innerHTML =
		`
			<div class="col-md-12 my-3">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title"><b><u>User Profile</u></b></h5>
						<p class="card-text text-left">Username: <b>${data.username}</b></p>
						<p class="card-text text-left">Email: <b>${data.email}</b></p>
						<p class="card-text text-left">First name: <b>${data.firstName}</b></p>
						<p class="card-text text-left">Last name: <b>${data.lastName}</b></p>
						<p class="card-text text-left">Mobile number: <b>${data.mobileNo}</b></p>
						<p class="card-text text-right">Status: <b>Inactive</b></p>
					</div>
				</div>
			</div>
		`

		coursesContainer =
		`
			<h5>User currently not active</h5>
		`
	}
	
})
	

